<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../conf/config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;

$exchange = 'work'; // 交换器名称
$queue = 'work'; // 队列名称
$consumerTag = 'work_tag_2'; // 消费者标签

// 建立一个到RabbitMQ服务器的连接
try {
    $connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST);
} catch (Exception $e) {
    die($e->getMessage());
}

// 建立通道
$channel = $connection->channel();
// 声明一个队列
$channel->queue_declare(
    $queue, // 队列名称
    false, //
    true, // 是否持久化
    false, // 是否排他
    false // 是否自动删除
);
// 声明一个交换机
$channel->exchange_declare(
    $exchange, // 交换器名称
    AMQPExchangeType::FANOUT, // 交换器类型，常见fanout,direct,topic,headers
    false, //
    true, // 是否持久化
    false // 是否自动删除
);
// 队列绑定到交换机
$channel->queue_bind(
    $queue, // 队列名称
    $exchange // 交换器名称
);

// 回调函数
echo " [*] Waiting for messages. To exit press CTRL+C\n";
$callback = function ($msg) {
    echo ' [x] Received ', $msg->body, "\n";
};

$channel->basic_consume(
    $queue, // 队列名称
    $consumerTag, // 消费者标签
    false, // 设置为true则表示不能将同一个connection中生产者发送的消息传递给这个connection中的消费者
    true, // 收到消息后，是否不需要回复确认即被认为被消费
    false, // 是否排他
    false, // 不返回执行结果,但是如果排他开启的话,则必须需要等待结果的,如果两个一起开就会报错
    $callback // 回调函数
);
while ($channel->is_consuming()) {
    $channel->wait();
}
$channel->close();
$connection->close();