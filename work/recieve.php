<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../conf/config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$queue = 'work'; // 队列名称

try {
    $connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST); // 建立连接到RabbitMQ服务器
    $channel = $connection->channel(); // 建立通道
    $channel->queue_declare($queue, false, false, false, false); // 试探性声明一个队列
    echo " [*] Waiting for messages. To exit press CTRL+C\n";
    $callback = function ($msg) { // 回调函数
        sleep(3);
        // 手动确认消息是否正常消费，保证消息消费的幂等。
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        echo ' [x] Received ', $msg->body, "\n";
    };
    // basic_qos方法设置参数prefetch_count = 1。这告诉RabbitMQ不要在一个时间给一个消费者多个消息（在处理和确认以前的消息之前，不要向消费者发送新消息。相反，它将发送给下一个仍然不忙的消费者）。
    $channel->basic_qos(null, 1, null);
    // basic_consume方法设置参数no_ack=false。告诉RabbitMQ消费消息需要手动确认。
    $channel->basic_consume($queue, '', false, false, false, false, $callback);
    while ($channel->is_consuming()) { // 循环获取消息
        $channel->wait();
    }
    $channel->close();
    $connection->close();
} catch (Exception $e) {
    die($e->getMessage());
}