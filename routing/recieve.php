<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../conf/config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;

$exchange = 'routing'; // 交换器名称

try {
    $connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST); // 建立连接到RabbitMQ服务器
    $channel = $connection->channel(); // 建立通道
    $channel->exchange_declare($exchange, AMQPExchangeType::DIRECT, false, false, false); // 试探性声明一个交换机
    // 创建了一个具有生成名称的非持久队 当我们连接上RabbitMQ的时候，我们需要一个全新的、空的队列。我们可以手动创建一个随机的队列名，或者让服务器为我们选择一个随机的队列名（推荐）
    list($queueName, ,) = $channel->queue_declare("", false, false, true, false); // 试探性声明一个队列
    $routingKeys = array_slice($argv, 1); // 获取routing_key
    if (empty($routingKeys)) throw new Exception('请输入至少一个routing_key'); // 输入routing_key
    foreach ($routingKeys as $routingKey) {
        $channel->queue_bind($queueName, $exchange, $routingKey); // 队列通过routing_key绑定交换机
    }
    echo " [*] Waiting for logs. To exit press CTRL+C\n";
    $callback = function ($msg) { // 回调函数
        sleep(3);
        // 手动确认消息是否正常消费，保证消息消费的幂等。
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        echo ' [x] Received ', $msg->delivery_info['routing_key'], ':', $msg->body, "\n";
    };
    $channel->basic_consume($queueName, '', false, false, false, false, $callback); // 消费消息
    while ($channel->is_consuming()) { // 循环消费消息，没有阻塞等待
        $channel->wait();
    }
    $channel->close(); // 通道关闭
    $connection->close(); // 连接关闭
} catch (Exception $e) {
    die($e->getMessage());
}