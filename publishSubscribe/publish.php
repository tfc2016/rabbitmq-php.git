<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../conf/config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;


$exchange = 'pubSub'; // 交换器名称

try {
    $connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST); // 建立连接到RabbitMQ服务器
    $channel = $connection->channel(); // 建立通道
    // 交换器类型定义fanout。
    $channel->exchange_declare($exchange, AMQPExchangeType::FANOUT, false, false, false); // 试探性声明一个交换机

    // 生产10条消息
    for ($i = 0; $i < 10; $i++) {
        $body = "发布/订阅模式下生成第【" . ($i + 1) . "】条消息";
        $message = new AMQPMessage($body, ['content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
        $channel->basic_publish($message, $exchange);
        echo ' [x] Sent ' . $body . "\n";
        sleep(1);
    }
    $channel->close(); // 关闭通道
    $connection->close(); // 关闭连接
} catch (Exception $e) {
    die($e->getMessage());
}