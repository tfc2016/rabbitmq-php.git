<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../conf/config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exchange\AMQPExchangeType;

$exchange = 'pubSub'; // 交换器名称

try {
    $connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST); // 建立连接到RabbitMQ服务器
    $channel = $connection->channel(); // 建立通道
    $channel->exchange_declare($exchange, AMQPExchangeType::FANOUT, false, false, false); // 试探性声明一个交换机
    // 创建了一个具有生成名称的非持久队 当我们连接上RabbitMQ的时候，我们需要一个全新的、空的队列。我们可以手动创建一个随机的队列名，或者让服务器为我们选择一个随机的队列名（推荐）
    list($queueName, ,) = $channel->queue_declare('', false, false, false, false); // 临时队列
    $channel->queue_bind($queueName, $exchange); // 队列绑定交换器

    echo " [*] Waiting for messages. To exit press CTRL+C\n";
    $callback = function ($msg) { // 回调函数
        sleep(3);
        // 手动确认消息是否正常消费，保证消息消费的幂等。
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        echo ' [x] Received ', $msg->body, "\n";
    };
    // basic_qos方法设置参数prefetch_count = 1。这告诉RabbitMQ不要在一个时间给一个消费者多个消息（在处理和确认以前的消息之前，不要向消费者发送新消息。相反，它将发送给下一个仍然不忙的消费者）。
    $channel->basic_qos(null, 1, null);
    $channel->basic_consume($queueName, '', false, false, false, false, $callback);
    while ($channel->is_consuming()) { // 循环获取消息
        $channel->wait();
    }
    $channel->close();
    $connection->close();
} catch (Exception $e) {
    die($e->getMessage());
}