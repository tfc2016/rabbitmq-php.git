<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../conf/config.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;

$queue = 'simple'; // 队列名称

try {
    $connection = new AMQPStreamConnection(HOST, PORT, USER, PASS, VHOST); // 建立连接到RabbitMQ服务器
    $channel = $connection->channel(); // 建立通道
    $channel->queue_declare($queue, false, false, false, false); // 试探性声明一个队列
    echo " [*] Waiting for messages. To exit press CTRL+C\n";
    $callback = function ($msg) { // 回调函数
        echo ' [x] Received ', $msg->body, "\n";
    };
    $channel->basic_consume($queue, '', true, true, false, false, $callback);
    while ($channel->is_consuming()) { // 循环获取消息
        $channel->wait();
    }
    $channel->close();
    $connection->close();
} catch (Exception $e) {
    die($e->getMessage());
}